import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class GameOfLifeTest {

    private void assertTwoDimensionalArrays(int[][] arrayOne, int[][] arrayTwo) {
        for (int i = 0; i < arrayOne.length; i++)
            assertArrayEquals(arrayOne[i], arrayTwo[i]);
    }

    @Test
    void checkBlockPatternStillLife() {
        int[][] liveCellCoordinates = {{1, 1}, {1, 2}, {2, 1}, {2, 2}};
        int[][] expectedLiveCellCoordinates = {{1, 1}, {1, 2}, {2, 1}, {2, 2}};
        Board board = new Board(5, 5);
        board.fill(liveCellCoordinates, GameOfLife.LIVE);
        GameOfLife game = new GameOfLife(board);

        game.start();
        int[][] result = game.getAliveCoordinates();

        assertTwoDimensionalArrays(expectedLiveCellCoordinates, result);
    }

    @Test
    void checkBoatPatternStillLife() {
        int[][] liveCellCoordinates = {{0, 1}, {1, 0}, {2, 1}, {0, 2}, {1, 2}};
        int[][] expectedLiveCellCoordinates = {{0, 1}, {0, 2}, {1, 0}, {1, 2}, {2, 1}};
        Board board = new Board(5, 5);
        board.fill(liveCellCoordinates, GameOfLife.LIVE);
        GameOfLife game = new GameOfLife(board);

        game.start();
        int[][] result = game.getAliveCoordinates();

        assertTwoDimensionalArrays(expectedLiveCellCoordinates, result);
    }

    @Test
    void checkBlinkerPatternOscillator() {
        int[][] liveCellCoordinates = {{1, 1}, {1, 0}, {1, 2}};
        int[][] expectedLiveCellCoordinates = {{0, 1}, {1, 1}, {2, 1}};
        Board board = new Board(5, 5);
        board.fill(liveCellCoordinates, GameOfLife.LIVE);
        GameOfLife game = new GameOfLife(board);

        game.start();
        int[][] result = game.getAliveCoordinates();

        assertTwoDimensionalArrays(expectedLiveCellCoordinates, result);
    }

    @Test
    void checkToadPatternTwoPhaseOscillator() {
        int[][] liveCellCoordinates = {{1, 1}, {1, 2}, {1, 3}, {2, 2}, {2, 3}, {2, 4}};
        int[][] expectedLiveCellCoordinates = {{0, 2}, {1, 1}, {1, 4}, {2, 1}, {2, 4}, {3, 3}};
        Board board = new Board(5, 5);
        board.fill(liveCellCoordinates, GameOfLife.LIVE);
        GameOfLife game = new GameOfLife(board);

        game.start();
        int[][] result = game.getAliveCoordinates();

        assertTwoDimensionalArrays(expectedLiveCellCoordinates, result);
    }

}
