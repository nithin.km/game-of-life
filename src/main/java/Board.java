import java.util.Arrays;

public class Board {
    private final int ROW;
    private final int COLUMN;
    private final int[][] layout;

    public Board(int ROW, int COLUMN) {
        this.ROW = ROW;
        this.COLUMN = COLUMN;
        layout = new int[ROW][COLUMN];
        fill(GameOfLife.DEAD);
    }

    public int getROW() {return ROW;}

    public int getCOLUMN() { return COLUMN;}

    public void fill(int value) {
        for (int[] row : layout) {
            Arrays.fill(row, value);
        }
    }

    public void fill(int[][] liveCoordinates, int status) {
        for (int[] row : liveCoordinates) {
            layout[row[0]][row[1]] = status;
        }
    }

    public int getCell(int row, int column) {
        return layout[row][column];
    }

    public void setCell(int row, int column, int value) {
        layout[row][column] = value;
    }

    public int getLiveNeighbours(int cellRow, int cellCol) {
        int liveNeighbours = 0;
        int rowEnd = Math.min(layout.length, cellRow + 2);
        int colEnd = Math.min(layout[0].length, cellCol + 2);

        for (int row = Math.max(0, cellRow - 1); row < rowEnd; row++) {
            for (int col = Math.max(0, cellCol - 1); col < colEnd; col++) {
                if ((row != cellRow || col != cellCol) && layout[row][col] == GameOfLife.LIVE) {
                    liveNeighbours++;
                }
            }
        }
        return liveNeighbours;
    }
}
