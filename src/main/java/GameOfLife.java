class GameOfLife {

    public final static int DEAD = 0;
    public final static int LIVE = 1;
    private final int ROW;
    private final int COLUMN;
    Board board;

    GameOfLife(Board board) {
        this.board = board;
        this.ROW = board.getROW();
        this.COLUMN = board.getCOLUMN();
    }

    public void start() {
        board = getNextBoard();
    }

    private Board getNextBoard() {
        Board nextGen = new Board(ROW, COLUMN);
        for (int row = 0; row < ROW; row++) {
            for (int col = 0; col < COLUMN; col++) {
                int newState = getNewCellState(board.getCell(row, col), board.getLiveNeighbours(row, col));
                nextGen.setCell(row, col, newState);
            }
        }
        return nextGen;
    }

    private int getNewCellState(int currentState, int liveNeighbours) {
        int newState = currentState;
        switch (currentState) {
            case LIVE:
                if (liveNeighbours < 2) {
                    newState = DEAD;
                }
                if (liveNeighbours > 3) {
                    newState = DEAD;
                }
                break;
            case DEAD:
                if (liveNeighbours == 3) {
                    newState = LIVE;
                }
                break;
        }
        return newState;
    }

    public int[][] getAliveCoordinates() {
        int[][] liveCellCoordinates = new int[10][2];
        int noOfLiveCoordinates = 0;
        for (int row = 0; row < ROW; row++) {
            for (int col = 0; col < COLUMN; col++) {
                if (board.getCell(row,col) == GameOfLife.LIVE) {
                    liveCellCoordinates[noOfLiveCoordinates][0] = row;
                    liveCellCoordinates[noOfLiveCoordinates][1] = col;
                    noOfLiveCoordinates++;
                }
            }
        }
        return liveCellCoordinates;
    }

}
